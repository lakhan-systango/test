var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');

app.use(cors());
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));


app.post('/client-request', function(req, res) {
    let data = req.body
	if (data) {
		res.status(200).json({
			message: 'success response',
			data: data,
		});
	} else {
		res.status(200).json({
			message: 'Data Not found.',
		});     
	}
});
app.get('/butler-response',function(req,res){
    let data= req.data
    if (data) {
		res.status(200).json({
			message: 'success response',
			data: data,
		});
	} else {
		res.status(200).json({
			message: 'Data Not found.',
		});     
	}

}) ;
var server = app.listen(8000, function() {
	var host = server.address().address;
	var port = server.address().port;
	console.log('listening at http://%s:%s', host, port);
});
